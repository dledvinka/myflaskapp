def Articles():
    articles = [
        {
            'id': 1,
            'title': 'Article One',
            'body': 'Some text',
            'author': 'David',
            'create_date':'04-25-2017'
        },
        {
            'id': 2,
            'title': 'Article Two',
            'body': 'Some text',
            'author': 'Julie',
            'create_date':'04-26-2017'
        },
        {
            'id': 3,
            'title': 'Article Three',
            'body': 'Some text',
            'author': 'Jiri',
            'create_date':'27-04-2017'
        }
    ]
    
    return articles